
caso1:
	./waf --run scratch/tp-redes-2tcp
	#gnuplot Imágenes/"CASO 1: 2 TCP"/tcp2_config_cwnd_ssthresh.plt
	#gnuplot tcp2_config_cwhnd_n2i0_ssthresh.plt
	#gnuplot tcp2_config_cwhnd_n3i0_ssthresh.plt
	#gnuplot tcp2_config_n2i0_inflight.plt
	#gnuplot tcp2_config_n3i0_inflight.plt
	#gnuplot tcp2_config_n2i0_rto.plt
	#gnuplot tcp2_config_n3i0_rto.plt
	#gnuplot tcp2_config_n2i0_rtt.plt
	#gnuplot tcp2_config_n3i0_rtt.plt

caso2:
	./waf --run scratch/tp-redes-2tcp_1udp
	#gnuplot 2tcp_1udp_config_cwnd_ssthresh.plt

caso3:
	./waf --run scratch/tp-redes-3tcp


caso4:
	./waf --run scratch/tp-redes-hybla
	#gnuplot hybla_config_cwnd_ssthresh.plt

all:
	./waf

configure:
	./waf configure --enable-examples --enable-tests

build:
	./waf build

install:
	./waf install

clean:
	./waf clean

distclean:
	./waf distclean
