set terminal png
set output "dumbbell-tp2-rto.png"
set xlabel "Time(seconds)"
set ylabel "rto"
set title "rto Plot"
plot "2tcp_1udp_dumbbell-tp2-20-rto.data" using 1:2 title "rto" with lines
