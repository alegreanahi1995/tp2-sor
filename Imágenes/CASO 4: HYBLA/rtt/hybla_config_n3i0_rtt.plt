set terminal png
set output "dumbbell-tp2-rtt3.png"
set xlabel "Time(seconds)"
set ylabel "rtt"
set title "rtt Plot"
plot "hybla_dumbbell-tp2-30-rtt.data" using 1:2 title "rtt" with lines
